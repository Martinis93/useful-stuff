/*
 * https://github.com/raihan2006i/google-map-inverse-circle
 * Copyright (c) 2013 Miah Raihan Mahmud Arman
 * Released under the MIT licence: http://opensource.org/licenses/mit-license
 * Note: The Google Maps API v3 must be included *before* this code
 */

function InvertedCircle(opts) {
  options = {
    visible: true,
    map: opts.map,
    center: opts.map.getCenter(),
    radius: 38000, // 200 km
    draggable: false,
    editable: false,
    stroke_weight: 2,
    stroke_color: "#ffffff",
    fill_opacity: 0.5,
    fill_color: "#ffffff",
    always_fit_to_map: true
  }
  options = this.extend_(options, opts);
  this.set('visible', options.visible);
  this.set('map', options.map);
  this.set('center', options.center);
  this.set('radius', options.radius);
  this.set('old_radius', options.radius);
  this.set('draggable', options.draggable);
  this.set('editable', options.editable);
  this.set('stroke_weight', options.stroke_weight);
  this.set('stroke_color', options.stroke_color);
  this.set('fill_opacity', options.fill_opacity);
  this.set('fill_color', options.fill_color);
  this.set('always_fit_to_map', options.always_fit_to_map);
  this.set('position', options.center);
  this.set('resize_leftright', options.resize_leftright);
  this.set('resize_updown', options.resize_updown);

  // TODO: We should be able to set custom center marker image icon
  
  

  this.drawCircle_(this.get('map'), this.get('position'), this.get('radius') / 1000);
  console.log(this.get('position'));

}

InvertedCircle.prototype = new google.maps.MVCObject();



InvertedCircle.prototype.setMap = function(map)
{
  this.set('map', map);
}

InvertedCircle.prototype.getMap = function()
{
  return this.get('map');
}

InvertedCircle.prototype.setVisible = function(visible)
{
 // this.set('visible', visible);
/*if(this.get('visible')){
      this.get('circleControlDiv').innerHTML = '<div style="direction: ltr; overflow: hidden; text-align: left; position: relative; color: rgb(0, 0, 0); font-family: Arial, sans-serif; -webkit-user-select: none; font-size: 13px; background-color: rgb(255, 255, 255); padding: 4px; border-width: 1px 1px 1px 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-top-color: rgb(113, 123, 135); border-right-color: rgb(113, 123, 135); border-bottom-color: rgb(113, 123, 135); -webkit-box-shadow: rgba(0, 0, 0, 0.4) 0px 2px 4px; box-shadow: rgba(0, 0, 0, 0.4) 0px 2px 4px; font-weight: bold; background-position: initial initial; background-repeat: initial initial; " title="Turn On/Off the Circle"><span><div style="width: 16px; height: 16px; overflow: hidden; position: relative; "><img style="position: absolute; left: 0px; top: 0px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; width: auto; height: auto; " src="http://maps.gstatic.com/mapfiles/drawing.png" draggable="false"></div></span></div>';
    }else{
      this.get('circleControlDiv').innerHTML = '<div style="direction: ltr; overflow: hidden; text-align: left; position: relative; color: rgb(51, 51, 51); font-family: Arial, sans-serif; -webkit-user-select: none; font-size: 13px; background-color: rgb(255, 255, 255); padding: 4px; border-width: 1px 1px 1px 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-top-color: rgb(113, 123, 135); border-right-color: rgb(113, 123, 135); border-bottom-color: rgb(113, 123, 135); -webkit-box-shadow: rgba(0, 0, 0, 0.4) 0px 2px 4px; box-shadow: rgba(0, 0, 0, 0.4) 0px 2px 4px; font-weight: normal; background-position: initial initial; background-repeat: initial initial; " title="Turn On/Off the Circle"><span><div style="width: 16px; height: 16px; overflow: hidden; position: relative; "><img style="position: absolute; left: 0px; top: -160px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; width: auto; height: auto; " src="http://maps.gstatic.com/mapfiles/drawing.png" draggable="false"></div></span></div>';
    }*/
};

InvertedCircle.prototype.getVisible = function()
{
  return this.get('visible');
};

InvertedCircle.prototype.setCenter = function(center)
{
  this.set('position', center);
};

InvertedCircle.prototype.getCenter = function()
{
  return this.get('position');
};

InvertedCircle.prototype.getRadius = function()
{
  return this.get('radius');
};

InvertedCircle.prototype.setRadius = function(radius)
{
  this.set('radius', radius);
};

InvertedCircle.prototype.getOldRadius = function()
{
  return this.get('old_radius');
};

InvertedCircle.prototype.setOldRadius = function(radius)
{
  this.set('old_radius', radius);
};


InvertedCircle.prototype.getEditable = function()
{
  return this.get('editable');
};

InvertedCircle.prototype.setEditable = function(editable)
{
  this.set('editable', editable);
};

InvertedCircle.prototype.getDraggable = function()
{
  return this.get('draggable');
};

InvertedCircle.prototype.setDraggable = function(draggable)
{
  this.set('draggable', draggable);
};

/**
   * Add the sizer markers to the map.
   *
   * @private
   */
InvertedCircle.prototype.addSizer_ = function() {
  
  var me = this;

};

/**
   * This is to draw Circle Visible Control button
   *
   * @private
   */
/**
   * This is to extend options
   *
   * @private
   */
InvertedCircle.prototype.extend_ = function(obj, extObj) {
  if (arguments.length > 2) {
    for (var a = 1; a < arguments.length; a++) {
      extend(obj, arguments[a]);
    }
  } else {
    for (var i in extObj) {
      obj[i] = extObj[i];
    }
  }
  return obj;
};

/**
   * This is draw spots
   * Thanks Sammy Hubner (http://www.linkedin.com/in/sammyhubner) for providing me these awesome code
   * @private
   */

InvertedCircle.prototype.drawSpot_ = function(point, radius) {
  var d2r = Math.PI / 180;   // degrees to radians
  var r2d = 180 / Math.PI;   // radians to degrees
  var earthsradius = 6371;   // 6371 is the radius of the earth in kilometers
  var ret = [];
  var isNearPrimaryMeridian = false;
  var dir = 1;
  var extp = [], start, end, i, theta, ex, ey;

  var points = 128;

  // find the radius in lat/lon
  var rlat = (radius / earthsradius) * r2d;
  var rlng = rlat / Math.cos(point.lat() * d2r);

  if (point.lng() > 0) {
    dir = -1;
  }


  if (dir==1) {
    start=0;
    end=points+1;
  } // one extra here makes sure we connect the
  else        {
    start=points+1;
    end=0;
  }
  for (i=start; (dir==1 ? i < end : i > end); i=i+dir)
  {
    theta = Math.PI * (i / (points/2));
    ex = point.lat() - 0.12 + (rlat * Math.sin(theta)); // center b + radius y * sin(theta)
    ey = point.lng() + 0.095 + (rlng * Math.cos(theta)); // center a + radius x * cos(theta)
    if ((dir === -1 && ey < 0) || (dir === 1 && ey > 0)) {
      ey = 0;
      isNearPrimaryMeridian = true;
    }
    extp.push(new google.maps.LatLng(ex, ey));
  }
  ret.push(extp);
  // if near primary meridian we have to draw an inverse
  if (isNearPrimaryMeridian) {
    extp = [];
    dir = -dir;
    if (dir==1) {
      start=0;
      end=points+1
    } // one extra here makes sure we connect the
    else        {
      start=points+1;
      end=0
    }
    for (i=start; (dir==1 ? i < end : i > end); i=i+dir)
    {
      theta = Math.PI * (i / (points/2));
      ex = point.lat() - 0.03 + (rlat * Math.sin(theta)); // center b + radius y * sin(theta)
      ey = point.lng() - 0.2 + (rlng * Math.cos(theta)); // center a + radius x * cos(theta)
      if ((dir === -1 && ey < 0) || (dir === 1 && ey > 0)) {
        ey = 0;
        isNearPrimaryMeridian = true;
      }
      extp.push(new google.maps.LatLng(ex, ey));
    }
    ret.push(extp);
  }
  return ret;
}

/**
   * The Overlay
   * Thanks Sammy Hubner (http://www.linkedin.com/in/sammyhubner) for providing me these awesome code
   *
   */

InvertedCircle.prototype.Overlay = function () {
  var latExtent = 86;
  var lngExtent = 180;
  var lngExtent2 = lngExtent - 1e-10;
  return [
  [
  new google.maps.LatLng(-latExtent, -lngExtent),  // left bottom
  new google.maps.LatLng(latExtent, -lngExtent),   // left top
  new google.maps.LatLng(latExtent, 0),            // right top
  new google.maps.LatLng(-latExtent, 0),           // right bottom
  ], [
  new google.maps.LatLng(-latExtent, lngExtent2),  // right bottom
  new google.maps.LatLng(latExtent, lngExtent2),   // right top
  new google.maps.LatLng(latExtent, 0),            // left top
  new google.maps.LatLng(-latExtent, 0),           // left bottom
  ]
  ];
}

/**
   * This is draw circle
   * Thanks Sammy Hubner (http://www.linkedin.com/in/sammyhubner) for providing me these awesome code
   * @private
   */
InvertedCircle.prototype.drawCircle_ = function(map, center, radius){

  var paths = new this.Overlay;

  var spot = this.drawSpot_(center, radius);
  for (var i = 0; i < spot.length; i++) {
    paths.push(spot[i]);
  }

  var donut = new google.maps.Polygon({
    strokeWeight: this.stroke_weight,
    strokeColor: this.stroke_color,
    fillColor: this.fill_color,
    fillOpacity: this.fill_opacity,
    map: map
  });
  
   var beachMarker = new google.maps.Marker({
    position: center,
    map: map,
    icon: '/images/map-pin.png'
  });
  this.set('paths', paths);
  this.set('donut', donut);
  if(this.getVisible())
    this.get('donut').setPaths(paths);
  


}
